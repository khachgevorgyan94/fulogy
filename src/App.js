import React, { Component } from 'react';
import { createBrowserHistory } from 'history';
import {  Route, Switch, Redirect, BrowserRouter } from 'react-router-dom';

import {
    MainCatalog,

} from './modules';

import Header from "./components/header";
import ROUTES from './platform/constants/routes';

import './assets/styles/index.scss';

class App extends Component {

  async componentDidMount() {
    //? For SSR to fully load Browser API (general for 'window')
    window.routerHistory = createBrowserHistory();
    window.routerHistory.listen(() => window.scrollTo(0, 0));
    this.setState({ generalAPILoaded: true });

    //? Backend initial data fetch
    // const success = await InitialStorage.fetchDefault();
    // if (success) this.setState({ initialStorageFetched: true });
    // else window.location.reload();

    // AuthController.CheckForActivation();
  }

  render() {


    return  (
        <BrowserRouter>
          <>
              <Header />
              <Switch>
                <Route exact path={ROUTES.CATALOG} component={MainCatalog} />
                <Redirect to={ROUTES.CATALOG} component={MainCatalog} />
              </Switch>
          </>
        </BrowserRouter>
    );
  }
}

export default App;
