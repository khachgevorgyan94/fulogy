import React, {Component} from 'react';
import './style.scss';
import './responsive.scss';
import ROUTES from "../../platform/constants/routes";
import {Link} from "react-router-dom";
import {ReactSVG} from 'react-svg'

import logoImg from "../../assets/images/svg/logo.svg";
import basketSVG from "../../assets/images/svg/basket.svg";

class Header extends Component {

    state = {
        isOpenMenu: false,
        animationStart: false,
        styleAnimation: {
            width: "1500px",
            height: '1500px',
            left: 0,
            top: 0
        },
        startAnimation: false
    };


    openMenu = () => {

        this.setState({isOpenMenu: !this.state.isOpenMenu},()=>{
            if(this.state.isOpenMenu){
                document.body.classList.add('body-fixed');
            }else{
                document.body.classList.remove('body-fixed');
            }
        })

    };
    closeMenu = () => {
        this.setState({isOpenMenu: false}, ()=>{
            if(this.state.isOpenMenu){
                document.body.classList.add('body-fixed');
            }else{
                document.body.classList.remove('body-fixed');
            }
        })
    };

    headerRef = React.createRef()

    clickAnimation = (e) => {
        this.setState({
            startAnimation: true,
            animationStart: !this.state.animationStart,
            styleAnimation: {
                width: "1500px",
                height: '1500px',
                left: e.pageX - this.headerRef.current.getBoundingClientRect().left - 1500 / 2 + 'px',
                top: e.pageY - this.headerRef.current.getBoundingClientRect().top - 1500 / 2 + 'px'
            }
        });

    };

    render() {
        const {styleAnimation} = this.state;
        return (
            <header>
                <div className="P-header-section" ref={this.headerRef}>
                    <div className='G-container'>
                        <div className="P-header-block G-flex G-align-center G-justify-between">
                        <span
                            className={`P-animation-click ${this.state.startAnimation ? this.state.animationStart ? " active-animate-1" : ' active-animate-2' : null} `}
                            style={{
                                left: `${styleAnimation.left}`,
                                top: `${styleAnimation.top}`,
                                width: `${styleAnimation.width}`,
                                height: `${styleAnimation.height}`,
                            }}/>
                            <div className="P-header-logo-basket G-flex G-align-center G-justify-between">
                                <Link to={ROUTES.CATALOG} className="G-header-logo" onClick={this.clickAnimation}>
                                    <ReactSVG src={logoImg}/>
                                </Link>
                                <div className='P-basket-button' onClick={this.clickAnimation}>
                                    <button>
                                        <ReactSVG src={basketSVG}/>
                                    </button>
                                </div>
                            </div>
                            <div className="header-menu-block G-flex G-flex-column G-justify-end G-align-end">
                                <div className={`header-menu ${this.state.isOpenMenu ? 'open-menu-react' : ''}`}>
                                    <div className="open-menu" onClick={() => {
                                        this.openMenu()
                                    }}>
                                        <span/>
                                        <span/>
                                        <span/>
                                        <span/>
                                    </div>
                                    <div className="for-mobile-bg" onClick={this.closeMenu}/>
                                    <div className="menu-cnt G-flex G-align-center G-justify-between mobile-bg-1">
                                        <div id="mySidenav" className="sidenav">
                                            <ul className="G-flex G-align-center">
                                                <li style={{transitionDelay: ` ${this.state.isOpenMenu ? " 0.2s" : '0s'}`}}>
                                                    <span data-link='Обучающее видео'>Обучающее видео</span>
                                                </li>
                                                <li style={{transitionDelay: ` ${this.state.isOpenMenu ? " 0.25s" : '0s'}`}}>
                                                    <span data-link='Оформление заказ'>Оформление заказ</span>
                                                </li>
                                                <li style={{transitionDelay: ` ${this.state.isOpenMenu ? " 0.3s" : '0s'}`}}>
                                                    <span data-link='Оплата'>Оплата</span>
                                                </li>
                                                <li style={{transitionDelay: ` ${this.state.isOpenMenu ? " 0.35s" : '0s'}`}}>
                                                    <span data-link='Доставка'>Доставка</span>
                                                </li>
                                                <li style={{transitionDelay: ` ${this.state.isOpenMenu ? " 0.4s" : '0s'}`}}>
                                                    <span data-link='Гарантия'>Гарантия </span>
                                                </li>
                                                <li style={{transitionDelay: ` ${this.state.isOpenMenu ? " 0.45s" : '0s'}`}}>
                                                    <span data-link='Возврат'>Возврат </span>
                                                </li>
                                                <li style={{transitionDelay: ` ${this.state.isOpenMenu ? " 0.5s" : '0s'}`}}>
                                                    <span data-link='Контакты'>Контакты </span>
                                                </li>
                                                <li style={{transitionDelay: ` ${this.state.isOpenMenu ? " 0.55s" : '0s'}`}}>
                                                    <span data-link='Партнерам'>Партнерам </span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        );
    }
}

export default Header;
