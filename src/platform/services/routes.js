//? Route Helper Service
//? The routing system has one more layer, please first read comments on /src/platform/decorators/routes.ts

import { matchPath } from 'react-router-dom';



class RouteService {

  //? Check route
  static isRoute = (route, exact = true) => !!matchPath(window.location.pathname, { path: route, exact });


}

export default RouteService;
