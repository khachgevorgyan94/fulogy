import React, {Component} from 'react';

import './style.scss';
import './responsive.scss';
import ProductImg from '../../assets/images/product.jpg'
import Slider from "react-slick";

class MainCatalog extends Component {
    state = {
        activeCatalog: 0,
        isOpenInfo: false,
        activeStep: 5
    };
    settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        afterChange: (index) => {
            this.setState({activeCatalog: index})
        },
    };

    dataProduct = {
        class: 'Standart',
        power: '59 Вт.',
        lightPower: '3459 Люмен = 7,5 ламп накаливания по 40 Вт.',
        date: '3 Года',
        montage: 'да',
        price: 2594,
        imgProduct: [
            {
                sliderImg: ProductImg,
                changeCatalogImg: ProductImg,
                changeProductType: 'Тёплый'
            },
            {
                sliderImg: ProductImg,
                changeCatalogImg: ProductImg,
                changeProductType: 'Дневной '
            },
            {
                sliderImg: ProductImg,
                changeCatalogImg: ProductImg,
                changeProductType: 'Холодный '
            }
        ]
    };

    componentDidMount() {
        this.changeStep(this.state.activeStep)
    }


    //  change catalog slider
    changeCatalog = (indexForSlider) => {
        this.setState({activeCatalog: indexForSlider});
        this.slider.slickGoTo(indexForSlider)
    };

    //  open and close  information
    openInfoSelectOption = () => {
        this.setState({isOpenInfo: true})
    };

    closeInfoSelectOption = () => {
        this.setState({isOpenInfo: false})
    };

    // change catalog steps
    changeStep = (number) => {
        this.setState({activeStep: number})
    };

    render() {
        const {isOpenInfo} = this.state;
        return (
            <>
                <section className="P-catalog-section">
                    <div className="G-container">
                        <div className="P-catalog-details-block G-flex">
                            <div className="P-slider-block">
                                <Slider {...this.settings} ref={slider => (this.slider = slider)}>
                                    {this.dataProduct.imgProduct.map((item, index) => {
                                        return <div className="P-slider-catalog-box" key={index}>
                                            <div className="P-slider-catalog-img"
                                                 style={{backgroundImage: `url('${item.sliderImg}')`}}/>
                                        </div>
                                    })}
                                </Slider>
                            </div>
                            <div className="P-catalog-detail-box">
                                {isOpenInfo ? <div className="P-info-catalog-box">
                                    <span onClick={this.closeInfoSelectOption}>вернуться </span>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam aut cupiditate
                                        inventore placeat. Accusamus assumenda consectetur deleniti deserunt dolor
                                        doloremque, dolores fuga illo, illum laborum nostrum officiis optio porro quam
                                        quibusdam ratione reiciendis repellendus repudiandae sint veniam veritatis,
                                        voluptas
                                        voluptates.</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur deleniti
                                        earum
                                        eveniet laboriosam laudantium magni modi, repellendus temporibus vero
                                        voluptates?
                                        Cum iste nesciunt nostrum numquam qui tempora. Consectetur cumque, delectus eius
                                        illum molestiae placeat quibusdam quisquam ratione voluptatum! Rem,
                                        repudiandae.</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae dolor, harum
                                        ipsa
                                        iusto laboriosam laborum magni nesciunt officiis quam, quod sit voluptatum.
                                        Accusamus corporis debitis earum excepturi exercitationem incidunt mollitia quas
                                        voluptates? Beatae doloribus et facere fuga iusto temporibus voluptatum.</p>
                                </div> : null}
                                <div className="P-catalog-info-block">
                                    <ul>
                                        <li>
                                            <p>Класс</p>
                                            <p>{this.dataProduct.class}</p>
                                        </li>
                                        <li>
                                            <p>Потребления мощность</p>
                                            <p>{this.dataProduct.power}</p>
                                        </li>
                                        <li>
                                            <p>Сила света </p>
                                            <p>{this.dataProduct.lightPower}</p>
                                        </li>
                                        <li>
                                            <p>Гарантия</p>
                                            <p>{this.dataProduct.date}</p>
                                        </li>
                                        <li>
                                            <p>Монтаж</p>
                                            <p>{this.dataProduct.montage}</p>
                                        </li>
                                        <li>
                                            <p>Итог сумма</p>
                                            <p>{this.dataProduct.price} рублей</p>
                                        </li>
                                    </ul>
                                </div>
                                <div className="P-about-details">
                                    <div className="P-about-detail-box G-flex G-align-center">
                                        <span className="P-click-info" onClick={this.openInfoSelectOption}>!</span>
                                        <div className="P-change-details">
                                            {/*<p>Выбери цвет свечения</p>*/}
                                            <span>{this.dataProduct.imgProduct.map((item, index) => {
                                                if (this.state.activeCatalog === index) {
                                                    return item.changeProductType
                                                }
                                                return true
                                            })}</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="P-change-catalog-details G-flex G-align-center">
                                    {this.dataProduct.imgProduct.map((item, index) => {
                                        return <div className="P-padding-details" key={index} onClick={() => {
                                            this.changeCatalog(index)
                                        }}>
                                            <div className="P-change-details-box"
                                                 style={{backgroundImage: `url('${item.changeCatalogImg}')`}}>
                                                {this.state.activeCatalog === index ?
                                                    <span className="P-checkbox"/> : null}
                                                <p>{item.changeProductType}</p>
                                            </div>
                                        </div>
                                    })}


                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="P-catalog-select-bg">
                    <div className="G-container">
                        <div className="P-change-catalog-props">
                            <ul className="G-flex">
                                <li onClick={() => {
                                    this.changeStep(0)
                                }} className={`${this.state.activeStep === 0 ? 'active-select' : ''}  
                                ${this.state.activeStep >= 0 ? 'change-step' : ''}`}>вариант кухни
                                </li>
                                <li onClick={() => {
                                    this.changeStep(1)
                                }} className={`${this.state.activeStep === 1 ? 'active-select' : ''}  
                                ${this.state.activeStep >= 1 ? 'change-step' : ''}`}>размеры
                                </li>
                                <li onClick={() => {
                                    this.changeStep(2)
                                }} className={`${this.state.activeStep === 2 ? 'active-select' : ''}  
                                ${this.state.activeStep >= 2 ? 'change-step' : ''}`}>сенсор питающий
                                </li>
                                <li onClick={() => {
                                    this.changeStep(3)
                                }} className={`${this.state.activeStep === 3 ? 'active-select' : ''}  
                                ${this.state.activeStep >= 3 ? 'change-step' : ''}`}>кабель блок
                                </li>
                                <li onClick={() => {
                                    this.changeStep(4)
                                }} className={`${this.state.activeStep === 4 ? 'active-select' : ''}  
                                ${this.state.activeStep >= 4 ? 'change-step' : ''}`}>питания
                                </li>
                                <li onClick={() => {
                                    this.changeStep(5)
                                }} className={`${this.state.activeStep === 5 ? 'active-select' : ''}  
                                ${this.state.activeStep >= 5 ? 'change-step' : ''}`}>цвет свечения
                                </li>
                                <li onClick={() => {
                                    this.changeStep(6)
                                }} className={`${this.state.activeStep === 6 ? 'active-select' : ''}  
                                ${this.state.activeStep >= 6 ? 'change-step' : ''}`}>монтаж
                                </li>
                                <li onClick={() => {
                                    this.changeStep(7)
                                }} className={`${this.state.activeStep === 7 ? 'active-select' : ''}  
                                ${this.state.activeStep >= 7 ? 'change-step' : ''}`}>корзина
                                </li>
                            </ul>
                        </div>
                    </div>
                </section>
            </>

        );
    }
}

export default MainCatalog;
